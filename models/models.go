package models
 
import (
	"time"
)
 
type (
    // Produk
    Produk struct {
        ID        int       `json:"id"`
        Nama      string    `name:"nama"`
        Stock     int    	`json:"stock"`
        Harga     int       `json:"harga"`
        Deskripsi string    `name:"deskripsi"`
        Terjual   int    	`json:"terjual"`
        // Foto   	  string    `name:"foto"`
        CreatedAt time.Time `json:"created_at"`
        UpdatedAt time.Time `json:"updated_at"`
    }
)

type (
    // Transaksi
    Transaksi struct {
        ID        	int     	`json:"id"`
        TotalHarga	int    		`json:"total_harga"`
        ProdukID  	int    		`json:"produk_id"`
        Pembeli     string     	`name:"pembeli"`
        Alamat	 	string  	`name:"deskripsi"`
        Status   	string   	`name:"status"`
    }
)

