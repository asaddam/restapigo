package main

import (
	"context"
    "log"
    "net/http"
    "github.com/crudgo3/pembeli"
    "github.com/crudgo3/penjual"
    "github.com/crudgo3/models"
	"github.com/crudgo3/utils"
	"encoding/json"
    "fmt"
    "strconv"
)

func GetProduk(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		ctx, cancel := context.WithCancel(context.Background())

		defer cancel()

		produks, err := produk.GetAllProduk(ctx)

		if err != nil {
			fmt.Println(err)
		}

		utils.ResponseJSON(w, produks, http.StatusOK)
		return
	}

	http.Error(w, "Tidak di ijinkan", http.StatusNotFound)
	return
}

// PostMahasiswa
func PostProduk(w http.ResponseWriter, r *http.Request) {
    if r.Method == "POST" {
 
        if r.Header.Get("Content-Type") != "application/json" {
            http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
            return
        }
 
        ctx, cancel := context.WithCancel(context.Background())
        defer cancel()
 
        var pdk models.Produk
 
        if err := json.NewDecoder(r.Body).Decode(&pdk); err != nil {
            utils.ResponseJSON(w, err, http.StatusBadRequest)
            return
        }
 
        if err := produk.InsertProduk(ctx, pdk); err != nil {
            utils.ResponseJSON(w, err, http.StatusInternalServerError)
            return
        }
 
        res := map[string]string{
            "status": "Succesfully",
        }
 
        utils.ResponseJSON(w, res, http.StatusCreated)
        return
	}
}

// PostMahasiswa
func PostTransaksi(w http.ResponseWriter, r *http.Request) {
    if r.Method == "POST" {
 
        if r.Header.Get("Content-Type") != "application/json" {
            http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
            return
        }
 
        ctx, cancel := context.WithCancel(context.Background())
        defer cancel()
 
        var trk models.Transaksi
 
        if err := json.NewDecoder(r.Body).Decode(&trk); err != nil {
            utils.ResponseJSON(w, err, http.StatusBadRequest)
            return
        }
 
        if err := transaksi.InsertBeli(ctx, trk); err != nil {
            utils.ResponseJSON(w, err, http.StatusInternalServerError)
            return
        }
 
        res := map[string]string{
            "status": "Succesfully",
        }
 
        utils.ResponseJSON(w, res, http.StatusCreated)
        return
	}
}

// CancelTransaksi
func CancelTransaksi(w http.ResponseWriter, r *http.Request) {

	if r.Method == "DELETE" {

		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()

		var trk models.Transaksi

        id := r.URL.Query().Get("id")

		if id == "" {
			utils.ResponseJSON(w, "id tidak boleh kosong", http.StatusBadRequest)
			return
		}
		trk.ID, _ = strconv.Atoi(id)

		if err := transaksi.Delete(ctx, trk); err != nil {

			kesalahan := map[string]string{
				"error": fmt.Sprintf("%v", err),
			}

			utils.ResponseJSON(w, kesalahan, http.StatusInternalServerError)
			return
		}

		res := map[string]interface{}{}
		res[`deletedRecord`] = trk
		res[`berhasil`] = transaksi.Delete(ctx, trk)

		utils.ResponseJSON(w, res, http.StatusOK)
		return
	}

	http.Error(w, "Tidak di ijinkan", http.StatusMethodNotAllowed)
	return
}

func main()  {
	http.HandleFunc("/produk", GetProduk)
	http.HandleFunc("/produk/create", PostProduk)
	http.HandleFunc("/transaksi/create", PostTransaksi)
	http.HandleFunc("/transaksi/cancel", CancelTransaksi)
    fmt.Println("Success")
 
    err := http.ListenAndServe(":6000", nil)
 
    if err != nil {
        log.Fatal(err)
    }
}