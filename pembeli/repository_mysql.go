package transaksi

import (
	"context"
    "fmt"
    "database/sql"
    "errors"
    "github.com/crudgo3/config"
    "github.com/crudgo3/models"
	"log"
	// "io"

)

const table = "transaksi"

func InsertBeli(ctx context.Context, trn models.Transaksi) error {
    db, err := config.MySQL()
 
    if err != nil {
        log.Fatal("Can't connect to MySQL", err)
    }
 
    queryText := fmt.Sprintf("INSERT INTO %v (total_harga, produk_id, pembeli, alamat, status) values(%v,%v,'%v','%v','%v')", table,
        trn.TotalHarga,
        trn.ProdukID,
        trn.Pembeli,
        trn.Alamat,
        trn.Status)
 
    _, err = db.ExecContext(ctx, queryText)
 
    if err != nil {
        return err
    }
    return nil
}



// Update
func Delete(ctx context.Context, trk models.Transaksi) error {

	db, err := config.MySQL()

	if err != nil {
		log.Fatal("Can't connect to MySQL", err)
	}

    queryText := fmt.Sprintf("DELETE FROM %v where id = '%d'", table, trk.ID)

	s, err := db.ExecContext(ctx, queryText)

	if err != nil && err != sql.ErrNoRows {
		return err
	}

	check, err := s.RowsAffected()
	fmt.Println(check)
	if check == 0 {
		return errors.New("id tidak ada")
	}

	return nil
}