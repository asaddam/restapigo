package produk

import (
	"context"
    "fmt"
    // "database/sql"
    // "errors"
    "github.com/crudgo3/config"
    "github.com/crudgo3/models"
    "log"
	"time"
	// "os"
	// "io"

)

const (
    table          = "produk"
    layoutDateTime = "2006-01-02 15:04:05"
)

// GetAll
func GetAllProduk(ctx context.Context) ([]models.Produk, error) {
 
    var produks []models.Produk
 
    db, err := config.MySQL()
 
    if err != nil {
        log.Fatal("Cant connect to MySQL", err)
    }
 
    queryText := fmt.Sprintf("SELECT * FROM %v ORDER BY id ASC", table)
 
    rowQuery, err := db.QueryContext(ctx, queryText)
 
    if err != nil {
        log.Fatal(err)
    }
 
    for rowQuery.Next() {
        var produk models.Produk
        var createdAt, updatedAt string
 
        if err = rowQuery.Scan(
			&produk.ID,
            &produk.Nama,
            &produk.Stock,
            &produk.Harga,
            &produk.Deskripsi,
            &produk.Terjual,
            // &produk.Foto,
            &createdAt,
            &updatedAt); err != nil {
            return nil, err
        }
 
        //  Change format string to datetime for created_at and updated_at
        produk.CreatedAt, err = time.Parse(layoutDateTime, createdAt)
 
        if err != nil {
            log.Fatal(err)
        }
 
        produk.UpdatedAt, err = time.Parse(layoutDateTime, updatedAt)
 
        if err != nil {
            log.Fatal(err)
        }
 
        produks = append(produks, produk)
    }
 
    return produks, nil
}

func InsertProduk(ctx context.Context, pdk models.Produk) error {
    db, err := config.MySQL()
 
    if err != nil {
        log.Fatal("Can't connect to MySQL", err)
    }
 
    queryText := fmt.Sprintf(`INSERT INTO %v (nama, stock, harga, deskripsi, terjual, created_at, updated_at) VALUES('%v',%v,%v,'%v',%v,'%v','%v')`, table,
        pdk.Nama,
        pdk.Stock,
        pdk.Harga,
        pdk.Deskripsi,
        pdk.Terjual,
        time.Now().Format(layoutDateTime),
        time.Now().Format(layoutDateTime) )
	fmt.Println(queryText)
    _, err = db.ExecContext(ctx, queryText)
 
    if err != nil {
        return err
    }
    return nil
}